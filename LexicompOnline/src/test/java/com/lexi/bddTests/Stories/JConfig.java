package com.lexi.bddTests.Stories;
import com.lexi.bddTests.Steps.*;
import java.util.List;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;



import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

	@RunWith(JUnitReportingRunner.class)
	public class JConfig extends JUnitStories {
		
		public JConfig() {
		configuredEmbedder().embedderControls().doGenerateViewAfterStories(true).doIgnoreFailureInStories(false)
		    .doIgnoreFailureInView(true).doVerboseFailures(true).useThreads(1).useStoryTimeouts("150000000");//.doFailOnStoryTimeout(true);
			JUnitReportingRunner.recommandedControls(configuredEmbedder());
			
			JUnitReportingRunner.recommandedControls(configuredEmbedder());
            configuredEmbedder().embedderControls()
            .useStoryTimeouts("15m")
            .doFailOnStoryTimeout(true);

		}
		
		@Override
		public Configuration configuration() {
			return new MostUsefulConfiguration()
			.useStoryLoader(new LoadFromClasspath(this.getClass()))
			.useStoryReporterBuilder(
					new StoryReporterBuilder().withDefaultFormats().withFormats(Format.CONSOLE, Format.HTML).withFailureTrace(true));
			
		}

		@Override
		public InjectableStepsFactory stepsFactory() {
			return new InstanceStepsFactory(configuration(),
					new LoginPageTestSteps());	
		}

		@Override
		protected List<String> storyPaths() {
			StoryFinder finder = new StoryFinder();
			return finder.findPaths(CodeLocations.codeLocationFromPath("src/test/resources"), "**/LoginPage.Story", null);
		}
		
	}
