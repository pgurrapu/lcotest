package com.lexi.bddTests.Steps;

import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.AfterStories;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import com.lexi.*;

public class LoginPageTestSteps {
	private static LoginPage loginObj;
	private static HomePage homeObj;
	static WebDriver driver;
	
	@AfterScenario
	public void afterSteps() {
		Page.quitDriver();
		//driver.quit();
	}
	
	@AfterStories
	public void afterStories() {
		Page.quitDriver();
		//driver.quit();
	}
	
	@Given("User is on the Login page")
	public void IamOnTheLoginPage() {
		loginObj = new LoginPage();	
		//driver=Page.driver;
	}
	
	@When("User enters valid USERNAME")
	public void validUsername() {
		loginObj.setUserNameElement(loginObj.username);
	}
		
	@When("User enters valid PASSWORD")
	public void validPassword() {
		loginObj.setUserPasswordElement(loginObj.password);
	}
		
	@When("User clicks on Login button")
	public void clickLoginButton() {
		loginObj.clickLoginButton();
	}
	
	@Then("User will be on Home Page $title")
	public void IWillBeOnTheHomedPage(@Named("title") String title) {
		homeObj = new HomePage(Page.driver);
		Assert.assertEquals(homeObj.getTitle(), title);

	}
		
	@Then("User will be on Home Page")
	public void IWillBeOnTheHomedPage() {
		homeObj = new HomePage(Page.driver);
		Assert.assertEquals(homeObj.getTitle(), "Wolters Kluwer Retail");
	}
}
