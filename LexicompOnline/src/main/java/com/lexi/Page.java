package com.lexi;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.lexi.Page;

public class Page {

		public static WebDriver driver;
		public static Properties myProperties;
		
		Page(){
			
			System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\lib\\chromedriver.exe");
			if( driver == null){
				driver = new ChromeDriver();
				driver.manage().window().maximize();
			}
		}
	   
		public void openPage(String url){
			driver.navigate().to(url);
		}

		public String getTitle(){
			return driver.getTitle();
		}
		
		public static void logout(){
			Page.pause(1000);
			WebElement logout =driver.findElement(By.linkText("Logout"));
			logout.click();
			driver.quit();
		}
		
		public static void quitDriver() {
			driver.quit();
		}
		
		@SuppressWarnings("unchecked")
		public static void readConfiguration(){
			String path = System.getProperty("user.dir");
			File configuration = new File(path + "/src/test/resources/com/lexi/configuration.json");
			myProperties = new Properties();  		 
			
			JSONParser parser = new JSONParser();
			try {
				Object obj = parser.parse(new FileReader(configuration) );
				JSONObject configObj = (JSONObject) obj;
				for(Iterator<String> i = configObj.keySet().iterator();  i.hasNext(); ){
					String key = i.next();
						myProperties.setProperty(key, (String)configObj.get(key));
				}
				
			} catch (IOException e) {
				System.err.println( e.getMessage());
			} catch(ParseException e1 ){
				System.err.println( e1.getMessage());
			}
		}
		
		public static void pause(long time){
			try {
				Thread.sleep(time);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	

}
