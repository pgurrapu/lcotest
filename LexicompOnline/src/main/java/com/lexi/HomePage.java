package com.lexi;

import org.openqa.selenium.WebDriver;

public class HomePage extends Page{
	
	private final String BLUE_BAR_HOMEPAGE_LNK = "home_link";
	private final String BLUE_BAR_INTERACTIONS_LNK = "interact_link";
	private final String BLUE_BAR_DRUGID_LNK = "drugid_link";
	private final String BLUE_BAR_CALCULATORS_LNK = "calc_link";
	private final String BLUE_BAR_PRODUCTAVAILABILITY_LNK = "pam_link";
	private final String BLUE_BAR_DRUGCOMPARISONS_LNK = "drug_comparisons_wrap";
	private final String BLUE_BAR_IVCOMPATIBILITY_LNK = "compat-tools";
	private final String BLUE_BAR_PATIENTEDUCATION_LNK = "pcm_link_wrap";
	private final String BLUE_BAR_FMS_LNK = "fms_link_wrap";
	private final String BLUE_BAR_TOXICOLOGY_LNK = "tox_link";
	private final String BLUE_BAR_VISUALDX_LNK = "vdx_link";
	private final String BLUE_BAR_DRUGPLANS_LNK = "drugplans_link";
	private final String BLUE_BAR_MORECLINICALTOOLS_LNK = "nav-tools";
	
	
	
	public HomePage(WebDriver adriver) {
		  driver=adriver;
	}
	
}
