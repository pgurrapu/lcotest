package com.lexi;

import org.openqa.selenium.By;
import com.lexi.Page;
import org.openqa.selenium.By;

public class LoginPage extends Page {

		private final String USERNAME_TXT = "login-field";
		private final String PASSWORD_TXT = "password-field";
		private final String LOGIN_BTN = "btn-login";
		
		public String username;
		public String password;
		
		public LoginPage() {
			super();
			//Page.driver = null;
			Page.readConfiguration();
			this.username = Page.myProperties.getProperty("username");
			this.password = Page.myProperties.getProperty("password");
			openPage(Page.myProperties.getProperty("url"));
			//login();
		}
		
		public LoginPage(String uname, String pword){
			super();
			this.username = uname;
			this.password = pword;
		}
		
		public void login(){
			driver.findElement(By.id(this.USERNAME_TXT)).sendKeys(this.username);
			driver.findElement(By.id(this.PASSWORD_TXT)).sendKeys(this.password);
			driver.findElement(By.id(this.LOGIN_BTN)).click();
		}
		
		public void setUserNameElement(String username){
			driver.findElement(By.id(this.USERNAME_TXT)).clear();
			driver.findElement(By.id(this.USERNAME_TXT)).sendKeys(username);
		}
		
		public void setUserPasswordElement(String password){
			driver.findElement(By.id(this.PASSWORD_TXT)).clear();
			driver.findElement(By.id(this.PASSWORD_TXT)).sendKeys(password);
		}
		
		public void clickLoginButton() {
			driver.findElement(By.id(this.LOGIN_BTN)).click();
		}
}

